#!/usr/bin/env ruby
exit unless 'sivers.org' == %x{hostname}.strip
require 'pstore'
require 'pg'
require 'nownownow-config.rb'
require 'twitter'

DB = PG::Connection.new(dbname: 'd50b', user: 'd50b')

# log of what's already been tweeted (or init)
logfile = '/var/www/tiny/nowtweets.pstore'
ps = PStore.new(logfile)
unless File.exist?(logfile)
	ps.transaction do
		ps[:log] = []
		ps[:log] << {id: 155, url: 'http://sivers.org/now', when: '2015-11-01 18:56:57 -0800'}
	end
end

# get array of ids that have been tweeted
ids_tweeted =	ps.transaction(true) do
	ps[:log].map {|x| x[:id]}
end

# turn into a string to tell query not these
notstring = ids_tweeted.join(',')

# get a random now.url whose person has a twitter URL
qry = "SELECT u.id, long,
	regexp_replace(p.url, 'http.*twitter.com/', '') AS twitter
	FROM now.urls u INNER JOIN peeps.urls p
		ON (u.person_id=p.person_id AND p.url LIKE '%twitter.com/%')
	WHERE u.id NOT IN (#{notstring})
	AND long IS NOT NULL ORDER BY RANDOM() LIMIT 1"
res = DB.exec(qry)
raise 'none left' unless res.ntuples == 1
id = res[0]['id'].to_i
url = res[0]['long']
twitter = res[0]['twitter']

# open up log to prepare to log it
ps.transaction do
	# make the tweet text
	tweet = 'Now: %s by @%s' % [url, twitter]
	# Twitter API client
	tw = Twitter::REST::Client.new do |config|
		config.consumer_key = TWITTER_CONSUMER_KEY
		config.consumer_secret = TWITTER_CONSUMER_SECRET
		config.access_token = TWITTER_ACCESS_TOKEN
		config.access_token_secret = TWITTER_ACCESS_SECRET
	end
	# show it
	puts tweet
	# tweet it!
	tw.update tweet
	# log it
	ps[:log] << {id: id, url: url, when: Time.now()}
end

